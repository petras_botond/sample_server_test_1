//
// Created by petras on 2020. 06. 02..
//

#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <database/db_handler.h>

QString err;
bool ok, hasError;

////////////////////////////////////////////////////////////
//
//  moc object for test constructor & init method
//
////////////////////////////////////////////////////////////

class moc_db_handler: public Db_handler{
public:
    moc_db_handler(const QString& driver, const QString& connectionName, const QString& dbName):
    Db_handler(driver,connectionName,dbName)
    {

    }

    QString getDbname(){
        return db.databaseName();
    }
    QString getDriver(){
        return db.driverName();
    }
    QString getConnection(){
        return db.connectionName();
    }
    bool isOpen(){
        return db.isOpen();
    }
};

TEST_CASE("Db_handler constructor") {
    moc_db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

    REQUIRE(test.getConnection() == "Sample");
    REQUIRE(test.isOpen());
    REQUIRE(test.getDbname().toStdString() == "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    REQUIRE(test.getDriver().toStdString() == "QSQLITE");
}

TEST_CASE("Db_handler tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

////////////////////////////////////////////////////////////
//
//  Test for Db_handler::get_all_user_slot
//
////////////////////////////////////////////////////////////

    SECTION("get_all_user_slot") {
        QVariantList allUser;

        test.get_all_user_slot(&allUser, &hasError);

        REQUIRE(!hasError);
        REQUIRE(allUser.size() == 2);

        REQUIRE(allUser.at(0).toMap().value("id").toString().toStdString() == "1");
        REQUIRE(allUser.at(1).toMap().value("id").toString().toStdString() == "2");
    }

////////////////////////////////////////////////////////////
//
//  Test for Db_handler::get_all_department_slot
//
////////////////////////////////////////////////////////////

    SECTION("get_all_department_slot") {
        QVariantList allUser;

        test.get_all_department_slot(&allUser, &hasError);

        REQUIRE(!hasError);
        REQUIRE(allUser.size() == 2);

        REQUIRE(allUser.at(0).toMap().value("name").toString().toStdString() == "system");
        REQUIRE(allUser.at(1).toMap().value("name").toString().toStdString() == "VIRT");
    }

}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::check_user_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("check_user_slot tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

    SECTION("check_user_slot valid user id") {
        test.check_user_slot("1", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(ok);
    }

    SECTION("check_user_slot empty user id") {
        test.check_user_slot("", &ok, &hasError);

        REQUIRE(hasError);
        REQUIRE(!ok);
    }

    SECTION("check_user_slot invalid user id") {
        test.check_user_slot("id = 10", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(!ok);
    }

    SECTION("check_user_slot invalid query") {
        test.check_user_slot("idx = 10", &ok, &hasError);

        REQUIRE(hasError);
        REQUIRE(ok);
    }
}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::validate_dep_id_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("validate_dep_id_slot tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

    SECTION("validate_dep_id_slot valid department id") {
        test.validate_dep_id_slot("2", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(ok);
    }

    SECTION("validate_dep_id_slot invalid department id") {
        test.validate_dep_id_slot("10", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(!ok);
    }

    SECTION("validate_dep_id_slot empty department id") {
        test.validate_dep_id_slot("", &ok, &hasError);

        REQUIRE(hasError);
        REQUIRE(!ok);
    }
}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::get_user_all_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("get_user_all_slot tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

    SECTION("get_user_all_slot valid user") {
        QVariantMap data;

        test.get_user_all_slot("1", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.size() == 7);

        for (const auto& it : data.values()) {
            REQUIRE(it.isValid());
            REQUIRE(!it.toString().isEmpty());
        }
    }

    SECTION("get_user_all_slot empty id") {
        QVariantMap data;

        test.get_user_all_slot("", &data, &hasError);

        REQUIRE(hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_user_all_slot invalid user") {
        QVariantMap data;

        test.get_user_all_slot("10", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }
}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::get_self_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("get_self_slot tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

    SECTION("get_self_slot valid user - id") {
        QVariantMap data;

        test.get_self_slot("1", "id", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.size() == 5);

        REQUIRE(data["name"].toString().toStdString() == "Default admin");
        REQUIRE(data["user_name"].toString().toStdString() == "admin");
        REQUIRE(data["email"].toString().toStdString() == "admin@admin.com");
        REQUIRE(data["neptun"].toString().toStdString() == "aaa111");
        REQUIRE(data["department_id"].toString().toStdString() == "1");
    }

    SECTION("get_self_slot valid user - user_name") {
        QVariantMap data;

        test.get_self_slot("admin", "user_name", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.size() == 5);

        REQUIRE(data["name"].toString().toStdString() == "Default admin");
        REQUIRE(data["user_name"].toString().toStdString() == "admin");
        REQUIRE(data["email"].toString().toStdString() == "admin@admin.com");
        REQUIRE(data["neptun"].toString().toStdString() == "aaa111");
        REQUIRE(data["department_id"].toString().toStdString() == "1");
    }

    SECTION("get_self_slot valid user - email") {
        QVariantMap data;

        test.get_self_slot("admin@admin.com", "email", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.size() == 5);

        REQUIRE(data["name"].toString().toStdString() == "Default admin");
        REQUIRE(data["user_name"].toString().toStdString() == "admin");
        REQUIRE(data["email"].toString().toStdString() == "admin@admin.com");
        REQUIRE(data["neptun"].toString().toStdString() == "aaa111");
        REQUIRE(data["department_id"].toString().toStdString() == "1");
    }

    SECTION("get_self_slot valid user - neptun") {
        QVariantMap data;

        test.get_self_slot("aaa111", "neptun", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.size() == 5);

        REQUIRE(data["name"].toString().toStdString() == "Default admin");
        REQUIRE(data["user_name"].toString().toStdString() == "admin");
        REQUIRE(data["email"].toString().toStdString() == "admin@admin.com");
        REQUIRE(data["neptun"].toString().toStdString() == "aaa111");
        REQUIRE(data["department_id"].toString().toStdString() == "1");
    }

    SECTION("get_self_slot full invalid type") {
        QVariantMap data;

        test.get_self_slot("1", "something", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_self_slot invalid type: id for user_name") {
        QVariantMap data;

        test.get_self_slot("petras", "id", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_self_slot invalid type: neptun for id") {
        QVariantMap data;

        test.get_self_slot("1", "neptun", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_self_slot invalid type: email for neptun") {
        QVariantMap data;

        test.get_self_slot("aaa111", "email", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_self_slot invalid type: user_name for email") {
        QVariantMap data;

        test.get_self_slot("admin@admin.com", "user_name", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_self_slot empty type") {
        QVariantMap data;

        test.get_self_slot("1", "", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_self_slot empty id") {
        QVariantMap data;

        test.get_self_slot("", "id", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_self_slot invalid user by user_name") {
        QVariantMap data;

        test.get_self_slot("user", "user_name", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_self_slot invalid user by id") {
        QVariantMap data;

        test.get_self_slot("10", "id", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_self_slot invalid user by neptun") {
        QVariantMap data;

        test.get_self_slot("abcdef", "neptun", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_self_slot invalid user by email") {
        QVariantMap data;

        test.get_self_slot("user@user.com", "email", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }
}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::check_admin_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("check_admin_slot tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

    SECTION("check_admin_slot valid admin permission") {
        test.check_admin_slot("admin", "admin", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(ok);
    }

    SECTION("check_admin_slot invalid admin permission") {
        test.check_admin_slot("petras", "petras3", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(!ok);
    }

    SECTION("check_admin_slot empty parameter user_name") {
        test.check_admin_slot("", "petras3", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(!ok);
    }

    SECTION("check_admin_slot empty parameter password") {
        test.check_admin_slot("petras", "", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(!ok);
    }
}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::get_other_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("get_other_slot tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

    SECTION("get_self_slot valid user - id") {
        QVariantMap data;

        test.get_self_slot("1", "id", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.size() == 5);

        REQUIRE(data["user_name"].toString().toStdString() == "admin");
        REQUIRE(data["neptun"].toString().toStdString() == "aaa111");
        REQUIRE(data["department_id"].toString().toStdString() == "1");
    }

    SECTION("get_self_slot valid user - user_name") {
        QVariantMap data;

        test.get_self_slot("admin", "user_name", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.size() == 5);

        REQUIRE(data["user_name"].toString().toStdString() == "admin");
        REQUIRE(data["neptun"].toString().toStdString() == "aaa111");
        REQUIRE(data["department_id"].toString().toStdString() == "1");
    }

    SECTION("get_self_slot valid user - neptun") {
        QVariantMap data;

        test.get_self_slot("aaa111", "neptun", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.size() == 5);

        REQUIRE(data["user_name"].toString().toStdString() == "admin");
        REQUIRE(data["neptun"].toString().toStdString() == "aaa111");
        REQUIRE(data["department_id"].toString().toStdString() == "1");
    }

    SECTION("get_other_slot full invalid type") {
        QVariantMap data;

        test.get_self_slot("1", "something", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_other_slot invalid type: id for user_name") {
        QVariantMap data;

        test.get_self_slot("petras", "id", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_other_slot invalid type: email for id") {
        QVariantMap data;

        test.get_self_slot("1", "email", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_other_slot invalid type: user_name for email") {
        QVariantMap data;

        test.get_self_slot("admin@admin.com", "user_name", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_other_slot empty type") {
        QVariantMap data;

        test.get_self_slot("1", "", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_other_slot empty id") {
        QVariantMap data;

        test.get_self_slot("", "id", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_other_slot invalid user by id") {
        QVariantMap data;

        test.get_self_slot("10", "id", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_other_slot invalid user by name") {
        QVariantMap data;

        test.get_self_slot("user", "user_name", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }

    SECTION("get_other_slot invalid user by neptun") {
        QVariantMap data;

        test.get_self_slot("abcdef", "neptun", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data.isEmpty());
    }
}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::get_login_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("get_login_slot tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

    SECTION("get_login_slot valid login - user_name") {
        test.get_login_slot("petras", "petras3", "user_name", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(ok);
    }

    SECTION("get_login_slot valid login - email") {
        test.get_login_slot("petras@petras.com", "petras3", "email", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(ok);
    }

    SECTION("get_login_slot invalid password - user_name") {
        test.get_login_slot("admin", "petras3", "user_name", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(!ok);
    }

    SECTION("get_login_slot invalid password - email") {
        test.get_login_slot("admin@admin.com", "petras3", "email", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(!ok);
    }

    SECTION("get_login_slot invalid type: user_name for email") {
        test.get_login_slot("admin@admin.com", "admin", "user_name", &ok, &hasError);

        REQUIRE(hasError);
    }

    SECTION("get_login_slot invalid type: email for user_name") {
        test.get_login_slot("admin", "admin", "email", &ok, &hasError);

        REQUIRE(hasError);
    }

    SECTION("get_login_slot full invalid type") {
        test.get_login_slot("admin@admin.com", "petras3", "petras", &ok, &hasError);

        REQUIRE(hasError);
    }

    SECTION("get_login_slot empty id") {
        test.get_login_slot("", "petras3", "user_name", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(!ok);
    }

    SECTION("get_login_slot empty password") {
        test.get_login_slot("admin@admin.com", "", "email", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(!ok);
    }

    SECTION("get_login_slot empty type") {
        test.get_login_slot("admin@admin.com", "admin", "", &ok, &hasError);

        REQUIRE(!hasError);
        REQUIRE(!ok);
    }
}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::get_id_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("get_id_slot tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();
    QString id;

    SECTION("get_user_id_slot valid user_name - user") {
        test.get_id_slot("user", "admin", "user_name", &id, &hasError);

        REQUIRE(!hasError);
        REQUIRE(id == "1");
    }

    SECTION("get_user_id_slot valid email - user") {
        test.get_id_slot("user", "admin@admin.com", "email", &id, &hasError);

        REQUIRE(!hasError);
        REQUIRE(id == "1");
    }

    SECTION("get_user_id_slot valid neptun - user") {
        test.get_id_slot("user", "aaa111", "neptun", &id, &hasError);

        REQUIRE(!hasError);
        REQUIRE(id == "1");
    }

    SECTION("get_user_id_slot valid name - department") {
        test.get_id_slot("department", "VIRT", "name", &id, &hasError);

        REQUIRE(!hasError);
        REQUIRE(id == "2");
    }

    SECTION("get_user_id_slot invalid user_name - user") {
        test.get_id_slot("user", "user", "user_name", &id, &hasError);

        REQUIRE(!hasError);
        REQUIRE(id == "-1");
    }

    SECTION("get_user_id_slot invalid email - user") {
        test.get_id_slot("user", "user@user.com", "email", &id, &hasError);

        REQUIRE(!hasError);
        REQUIRE(id == "-1");
    }

    SECTION("get_user_id_slot invalid neptun - user") {
        test.get_id_slot("user", "abcdef", "neptun", &id, &hasError);

        REQUIRE(!hasError);
        REQUIRE(id == "-1");
    }

    SECTION("get_user_id_slot invalid name - department") {
        test.get_id_slot("department", "user", "name", &id, &hasError);

        REQUIRE(!hasError);
        REQUIRE(id == "-1");
    }

    SECTION("get_user_id_slot empty user name") {
        test.get_id_slot("user", "", "user_name", &id, &hasError);

        REQUIRE(!hasError);
        REQUIRE(id == "");
    }
}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::check_reg_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("check_reg_slot tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

    SECTION("check_reg_slot valid user") {
        test.check_reg_slot("User", "user@user.com", "abc123", &err, &hasError);

        REQUIRE(!hasError);
        REQUIRE(err.isEmpty());
    }

    SECTION("check_reg_slot invalid user_name") {
        test.check_reg_slot("admin", "user@user.com", "abc123", &err, &hasError);

        REQUIRE(!hasError);
        REQUIRE(err.toStdString() == "user_name");
    }

    SECTION("check_reg_slot invalid email") {
        test.check_reg_slot("user", "admin@admin.com", "abc123", &err, &hasError);

        REQUIRE(!hasError);
        REQUIRE(err.toStdString() == "email");
    }

    SECTION("check_reg_slot invalid neptun") {
        test.check_reg_slot("user", "user@user.com", "aaa111", &err, &hasError);

        REQUIRE(!hasError);
        REQUIRE(err.toStdString() == "neptun");
    }

    SECTION("check_reg_slot invalid user_name & email") {
        test.check_reg_slot("admin", "admin@admin.com", "abc123", &err, &hasError);

        REQUIRE(!hasError);
        REQUIRE(err.toStdString() == "user_name");
    }

    SECTION("check_reg_slot invalid user_name & neptun") {
        test.check_reg_slot("admin", "user@user.com", "aaa111", &err, &hasError);

        REQUIRE(!hasError);
        REQUIRE(err.toStdString() == "user_name");
    }

    SECTION("check_reg_slot invalid email & neptun") {
        test.check_reg_slot("user", "admin@admin.com", "aaa111", &err, &hasError);

        REQUIRE(!hasError);
        REQUIRE(err.toStdString() == "email");
    }

    SECTION("check_reg_slot empty parameter user_name") {
        test.check_reg_slot("", "user@user.com", "abc123", &err, &hasError);

        REQUIRE(!hasError);
        REQUIRE(err.toStdString() == "empty_user_name");
    }

    SECTION("check_reg_slot empty parameter email") {
        test.check_reg_slot("user", "", "abc123", &err, &hasError);

        REQUIRE(!hasError);
        REQUIRE(err.toStdString() == "empty_email");
    }

    SECTION("check_reg_slot empty parameter neptun") {
        test.check_reg_slot("user", "user@user.com", "", &err, &hasError);

        REQUIRE(!hasError);
        REQUIRE(err.toStdString() == "empty_neptun");
    }
}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::add_department_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("add_department_slot tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

    SECTION("valid new department") {
        QString id;

        test.get_id_slot("department", "RSZT", "name", &id, &hasError);

        REQUIRE(!hasError);
        REQUIRE(id.toStdString() == "-1");

        test.add_department_slot("RSZT", &hasError);

        test.get_id_slot("department", "RSZT", "name", &id, &hasError);

        REQUIRE(!hasError);
        REQUIRE(id.toStdString() != "-1");
    }

    SECTION("invalid new department") {
        QString id;

        test.get_id_slot("department", "RSZT", "name", &id, &hasError);

        REQUIRE(!hasError);
        REQUIRE(id.toStdString() != "-1");

        test.add_department_slot("RSZT", &hasError);

        REQUIRE(hasError);
    }

    SECTION("empty department") {
        test.add_department_slot("", &hasError);

        REQUIRE(hasError);
    }
}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::set_dep_id_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("set_dep_id_slot tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

    SECTION("valid new department id") {
        QVariantMap data;

        test.get_user_all_slot("2", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data["department_id"].toString().toStdString() == "2");

        test.set_dep_id_slot("2", "3", &hasError);

        REQUIRE(!hasError);

        test.get_user_all_slot("2", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data["department_id"].toString().toStdString() == "3");
    }

    SECTION("invalid new department id") {
        QVariantMap data;

        test.get_user_all_slot("2", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data["department_id"].toString().toStdString() == "3");

        test.set_dep_id_slot("2", "5", &hasError);

        REQUIRE(hasError);
    }

    SECTION("empty department") {
        QVariantMap data;

        test.get_user_all_slot("2", &data, &hasError);

        REQUIRE(!hasError);
        REQUIRE(data["department_id"].toString().toStdString() == "3");

        test.set_dep_id_slot("2", "", &hasError);

        REQUIRE(hasError);
    }
}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::set_password_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("set_password_slot tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

    SECTION("valid call") {
        test.get_login_slot("petras", "petras3", "user_name", &ok, &hasError);

        REQUIRE(ok);
        REQUIRE(!hasError);

        test.set_password_slot("2", "petras", "petras", &hasError);

        REQUIRE(!hasError);

        test.get_login_slot("petras", "petras3", "user_name", &ok, &hasError);

        REQUIRE(!ok);
        REQUIRE(!hasError);

        test.get_login_slot("petras", "petras", "user_name", &ok, &hasError);

        REQUIRE(ok);
        REQUIRE(!hasError);
    }

    SECTION("id mismatch") {
        test.get_login_slot("petras", "petras", "user_name", &ok, &hasError);

        REQUIRE(ok);
        REQUIRE(!hasError);

        test.set_password_slot("2", "admin", "petras3", &hasError);

        REQUIRE(hasError);
    }

    SECTION("empty id parameter") {
        test.get_login_slot("petras", "petras", "user_name", &ok, &hasError);

        REQUIRE(ok);
        REQUIRE(!hasError);

        test.set_password_slot("", "admin", "petras3", &hasError);

        REQUIRE(hasError);
    }

    SECTION("empty user_name parameter") {
        test.get_login_slot("petras", "petras", "user_name", &ok, &hasError);

        REQUIRE(ok);
        REQUIRE(!hasError);

        test.set_password_slot("2", "", "petras3", &hasError);

        REQUIRE(hasError);
    }

    SECTION("empty password parameter") {
        test.get_login_slot("petras", "petras", "user_name", &ok, &hasError);

        REQUIRE(ok);
        REQUIRE(!hasError);

        test.set_password_slot("2", "admin", "", &hasError);

        REQUIRE(hasError);
    }
}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::set_reg_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("set_reg_slot tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

    QString local;

    SECTION("valid call") {
        test.check_reg_slot("user", "user@user.com", "abcdef", &local, &hasError);

        REQUIRE(local.isEmpty());
        REQUIRE(!hasError);

        test.validate_dep_id_slot("3", &ok, &hasError);

        REQUIRE(ok);
        REQUIRE(!hasError);

        test.set_reg_slot("User", "user", "user000", "user@user.com", "abcdef", "3", &hasError);

        REQUIRE(!hasError);

        test.get_login_slot("user", "user000", "user_name", &ok, &hasError);

        REQUIRE(ok);
        REQUIRE(!hasError);
    }

    SECTION("empty name") {

        test.set_reg_slot("", "user", "user000", "user@user.com", "abcdef", "3", &hasError);

        REQUIRE(hasError);
    }

    SECTION("empty user_name") {

        test.set_reg_slot("User", "", "user000", "user@user.com", "abcdef", "3", &hasError);

        REQUIRE(hasError);
    }

    SECTION("empty password") {

        test.set_reg_slot("User", "user", "", "user@user.com", "abcdef", "3", &hasError);

        REQUIRE(hasError);
    }

    SECTION("empty email") {

        test.set_reg_slot("User", "user", "user000", "", "abcdef", "3", &hasError);

        REQUIRE(hasError);
    }

    SECTION("empty neptun") {

        test.set_reg_slot("User", "user", "user000", "user@user.com", "", "3", &hasError);

        REQUIRE(hasError);
    }

    SECTION("empty dep_id") {

        test.set_reg_slot("", "user", "user000", "user@user.com", "abcdef", "", &hasError);

        REQUIRE(hasError);
    }

    SECTION("all parameters are empty") {

        test.set_reg_slot("", "", "", "", "", "", &hasError);

        REQUIRE(hasError);
    }
}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::clear_db_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("Db_handler11 tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::delete_from_db_slot
//
////////////////////////////////////////////////////////////

TEST_CASE("Db_handler12 tests") {
    Db_handler test("QSQLITE", "Sample", "/home/petras/Dokumentumok/git_repos/sample_server/sample.db");
    test.init();

}

////////////////////////////////////////////////////////////
//
//  Tests for Db_handler::
//
////////////////////////////////////////////////////////////
