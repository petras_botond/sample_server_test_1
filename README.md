This is a sample designed code set for reference to the later implemented API servers of myself.
The code has been planned over commented because of educational usage.

Structure:

    Main program (QCoreApplication main thread)
    
    Main server instance for handle http requests
    
    Manager server instance for local organize local execution
    
    Additional exec instances e.g. database handling instance  

Docker:
From project directory (locate of this file): 

    docker image build -t sample-server:latest .

    docker container run -it -p 3000:3000 --rm sample-server

Request documentation: [requests](./documentation/requests.md)
