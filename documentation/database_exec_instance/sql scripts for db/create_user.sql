create table user
(
    id integer
        constraint user_pk
            primary key autoincrement,
    name text not null,
    user_name text not null,
    password text not null,
    email text not null,
    neptun text not null,
    department_id int not null
        constraint user_department_id_fk
            references department
);

create unique index user_email_uindex
    on user (email);

create unique index user_id_uindex
    on user (id);

create unique index user_neptun_uindex
    on user (neptun);

create unique index user_user_name_uindex
    on user (user_name);
