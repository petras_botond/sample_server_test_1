create table department
(
	id integer
		constraint department_pk
			primary key autoincrement,
	name text not null
);

create unique index department_id_uindex
	on department (id);

create unique index department_name_uindex
	on department (name);

insert into department (name) values ('system')
