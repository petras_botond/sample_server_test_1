Requests for sample-server:
   
    <name>:<method:/resource>
    <request json>
    <response json>
    .
    .
    .
    <response json>

Add department (admin):POST:/dp --ok

    {
        "user_name":"admin",
        "password":"Admin123",
        "dep_name":"math"
    }
    
    {
    
    }

Set dep_id (admin):POST:/user/dp/<id> --ok

    {
        "user_name":"admin",
        "password":"Admin123",
        "dep_id":"1"
    }
    
    {
    
    }

Register:POST:/user/registration --ok
    
    {
        "name":"Test User",
        "user_name":"User".
        "password":"user",
        "email":"user@sample.com",
        "neptun":"abc123",
        "department_id":"1"
    }
    
    {
        "id":"2"
    }
    
    {
        
    }

Login:POST:/user/login --ok

    {
        "user_name":"User",
        "password":"user"
    }
    
    {
        "id":"2"
    }
    
    {
        
    }
    
Login by email:POST:/user/login/email --ok
    
    {
        "email":"User@sample.com",
        "password":"user"
    }
        
    {
        "id":"2"
    }
        
    {
            
    }

Set password:POST:/user/passw --ok

    {
        "user_name":"User",
        "old_passw":"user",
        "new_passw":"user123"
    }
    
    {
    
    }

Get self:POST:/user/self/<id> --ok

    {
        "user_name":"User",
        "password":"user"
    }
    
    {
        "name":"Test User",
        "user_name":"User".
        "email":"user@sample.com",
        "neptun":"abc123",
        "department_id":"1"
    }

Get other:GET:/user/<id> --ok

    {
    
    }
    
    {
        "user_name":"User".
        "neptun":"abc123",
        "department_id":"1"
    }

All users (admin):POST:/user/all --ok

    {
        "user_name":"admin",
        "password":"admin"
    }
    
    {
        [
            {
                "name":"Default admin",
                "user_name":"admin",
                "id":"1",
                "email":"admin@admin.com",
                "neptun":"aaa111",
                "department_id":"1"
                "department_name":"system"
            },
            .
            .
            .
            {...}
        ]
    }

All data from user (admin):POST:/user/all/<id> --ok

    {
            "user_name":"admin",
            "password":"admin"
    }
    
    {
         "name":"Default admin",
         "user_name":"admin",
         "id":"1",
         "email":"admin@admin.com",
         "neptun":"aaa111",
         "department_id":"1"
         "department_name":"system"
    }

All departments (admin):POST:/dp/all --ok

    {
        "user_name":"admin",
        "password":"admin"
    }

    {
        [
            {
                "department_id":"1"
                "department_name":"system"
            },
            .
            .
            .
            {...}
        ]
    }

Delete user (admin):POST:/user/delete --ok

    {
        "user_name":"admin",
        "password":"admin",
        "id":"2",
        "type":"id"
    }
    
    {
        "id":"2",
        "delete":"success"
    }

Delete department (admin):POST:/dp/delete --ok

    {
        "user_name":"admin",
        "password":"admin",
        "id":"2",
        "type":"id" //id, email, user_name, neptun
    }
    
    {
        "id":"2",
        "delete":"success"
    }

Clear db (admin):POST:/clear --ok

    {
        "user_name":"admin",
        "password":"admin"
    }
        
    {
        "user":["3","4","5"],
        "department":["2",3","4","5"],
        "delete":"success"
    }

Delete self account:POST:/user/delete/<id> --ok

    {
        "user_name":"petras",
        "password":"petras3"
    }
        
    {
        "delete":"success"
    }
