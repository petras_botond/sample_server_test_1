FROM ubuntu:latest

EXPOSE 3000

RUN apt update && apt upgrade -y
RUN apt install -y build-essential cmake qt5-default

COPY . /source

RUN mkdir /source/build

WORKDIR /source/build
RUN cmake .. && make -j4 && cp src/sample / && rm -r /source

ENTRYPOINT ["/sample"]
