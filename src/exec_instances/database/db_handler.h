//
// Created by petras on 2020. 05. 27..
//

#pragma once

#include <iostream>
#include <mutex>

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlDriver>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>

#include <QDateTime>
#include <QVariant>
#include <QThread>

class Db_handler: public QThread {
public:

////////////////////////////////////////////////////////////
//
//  Local functions e.g. constructor
//
////////////////////////////////////////////////////////////

    Db_handler(const QString& driver, const QString& connectionName, QString  dbName_);

    bool init();

    ~Db_handler() override;

public slots:

////////////////////////////////////////////////////////////
//
//  Execution slots
//
////////////////////////////////////////////////////////////

    void get_all_user_slot(QVariantList *data, bool *hasError);
    void get_all_department_slot(QVariantList *data, bool *hasError);
    void check_user_slot(const QString &query, bool *ok, bool *hasError);
    void validate_dep_id_slot(const QString &id, bool *ok, bool *hasError);
    void get_user_all_slot(const QString &id, QVariantMap *data, bool *hasError);
    void get_self_slot(const QString &id, const QString &type, QVariantMap *selfData, bool *hasError);
    void check_admin_slot(const QString &user_name, const QString &password, bool *ok, bool *hasError);
    void get_other_slot(const QString &id, const QString &type, QVariantMap *selfData, bool *hasError);
    void get_login_slot(const QString &id, const QString &passw, const QString &type, bool *ok, bool *hasError);
    void get_id_slot(const QString &table, const QString &parameter, const QString &type, QString *id, bool *hasError);
    void check_reg_slot(const QString &user_name, const QString &email, const QString &neptun, QString *error, bool *hasError);

    void add_department_slot(const QString &dep_name, bool *hasError);
    void set_dep_id_slot(const QString &user_id, const QString &dep_id, bool *hasError);
    void set_password_slot(const QString &user_id, const QString &user_name, const QString &password, bool *hasError);
    void set_reg_slot(const QString &name, const QString &user_name, const QString &password, const QString &email, const QString &neptun, const QString &dep_id, bool *hasError);

    void clear_db_slot(QVariantList *ids, QVariantList *deps, bool *hasError);
    void delete_from_db_slot(const QString &table, const QString &query, bool *hasError);

protected:

////////////////////////////////////////////////////////////
//
//  Local class variables
//
////////////////////////////////////////////////////////////

    std::mutex db_m;
    QString dbName;

    QSqlDatabase db;

    QSqlQuery getSelfQuery;
    QSqlQuery checkRegQuery;
    QSqlQuery getLoginQuery;
    QSqlQuery getOtherQuery;
    QSqlQuery checkUserQuery;
    QSqlQuery checkAdminQuery;
    QSqlQuery getAllUserQuery;
    QSqlQuery getUserAllQuery;
    QSqlQuery getUserByNameQuery;
    QSqlQuery validateDepIdQuery;

    QSqlQuery dbWriteQuery;

};
