//
// Created by petras on 2020. 05. 27..
//

#include "db_handler.h"

#include <utility>

#include <QRegExp>

Db_handler::Db_handler(const QString &driver, const QString& connectionName, QString  dbName_):
db(QSqlDatabase::addDatabase(driver, connectionName)),
dbName(std::move(dbName_)),
getUserByNameQuery(db),
validateDepIdQuery(db),
getAllUserQuery(db),
getUserAllQuery(db),
checkAdminQuery(db),
checkUserQuery(db),
getLoginQuery(db),
getOtherQuery(db),
checkRegQuery(db),
dbWriteQuery(db),
getSelfQuery(db)
{
    std::cout << "[Db_handler] Log: Started" << std::endl;

    this->moveToThread(this->thread());
}

bool Db_handler::init() {
    db.setDatabaseName(dbName);

    if (!db.open())
    {
        std::cout << "ERROR [Database - init] " << db.lastError().text().toStdString() << std::endl;
        return false;
    }
    return true;
}

Db_handler::~Db_handler() {
    quit();
    wait();
    moveToThread(nullptr);
}


void Db_handler::get_all_user_slot(QVariantList *data, bool *hasError) {
    QVariantMap oneUser;

    getAllUserQuery.clear();

    if(!getAllUserQuery.exec("SELECT user.name, user_name, user.id, email, neptun, user.department_id, department.name"
                             " FROM user JOIN department ON user.department_id=department.id"))
    {
        std::cout << "[Database::get_all_user]  Error: " << getAllUserQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    while (getAllUserQuery.next()) {
        oneUser.clear();

        oneUser["name"] = getAllUserQuery.value(0);
        oneUser["user_name"] = getAllUserQuery.value(1);
        oneUser["id"] = getAllUserQuery.value(2);
        oneUser["email"] = getAllUserQuery.value(3);
        oneUser["neptun"] = getAllUserQuery.value(4);
        oneUser["department_id"] = getAllUserQuery.value(5);
        oneUser["department_name"] = getAllUserQuery.value(6);

        data->push_back(oneUser);
    }

    *hasError = false;
}
//tested

void Db_handler::get_all_department_slot(QVariantList *data, bool *hasError) {

    QVariantMap oneUser;

    getAllUserQuery.clear();

    if(!getAllUserQuery.exec("SELECT id, name FROM department"))
    {
        std::cout << "[Database::get_all_department]  Error: " << getAllUserQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    while (getAllUserQuery.next()) {
        oneUser.clear();

        oneUser["id"] = getAllUserQuery.value(0);
        oneUser["name"] = getAllUserQuery.value(1);

        data->push_back(oneUser);
    }
}
//tested

void Db_handler::check_user_slot(const QString &query, bool *ok, bool *hasError) {

    *ok = true;
    *hasError = false;

    if (query.isEmpty())
    {
        *hasError = true;
        *ok = false;
        return;
    }

    checkUserQuery.clear();

    if(!checkUserQuery.exec("SELECT COUNT(*) FROM user WHERE " + query))
    {
        std::cout << "[Database::check_user]  Error: " << checkUserQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    checkUserQuery.next();

    if (checkUserQuery.value(0).toInt() == 0)
    {
        *ok = false;
        return;
    }
}
//tested

void Db_handler::validate_dep_id_slot(const QString &id, bool *ok, bool *hasError) {
    if (id.isEmpty())
    {
        *ok = false;
        *hasError = true;
        return;
    }

    *ok = true;
    *hasError = false;

    validateDepIdQuery.clear();

    if(!validateDepIdQuery.exec("SELECT COUNT(*) FROM department WHERE id = " + id))
    {
        std::cout << "[Database::validate_dep_id]  Error: " << validateDepIdQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    validateDepIdQuery.next();

    if (validateDepIdQuery.value(0).toInt() != 1)
    {
        *ok = false;
        return;
    }
}
//tested

void Db_handler::get_user_all_slot(const QString &id, QVariantMap *data, bool *hasError) {
    data->clear();

    if (id.isEmpty())
    {
        *hasError = true;
        return;
    }

    *hasError = false;

    bool ok, localError;

    check_user_slot("id = " + id, &ok, &localError);

    if (localError)
    {
        std::cout << "[Db_handler] Error: From Db_server - check_user" << std::endl;
        *hasError = true;
        return;
    }

    if (!ok)
    {
        std::cout << "[Db_handler] Error: From Db_server - No matching user" << std::endl;
        return;
    }

    getUserAllQuery.clear();

    if(!getUserAllQuery.exec("SELECT user.name, user_name, user.id, email, neptun, user.department_id, department.name "
                             "FROM user JOIN department ON user.department_id=department.id WHERE user.id = " + id))
    {
        std::cout << "[Database::get_user_all]  Error: " << getUserAllQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    getUserAllQuery.next();

    data->insert("name",getUserAllQuery.value(0).toString());
    data->insert("user_name",getUserAllQuery.value(1).toString());
    data->insert("id",getUserAllQuery.value(2).toString());
    data->insert("email",getUserAllQuery.value(3).toString());
    data->insert("neptun", getUserAllQuery.value(4).toString());
    data->insert("department_id", getUserAllQuery.value(5).toString());
    data->insert("department_name", getUserAllQuery.value(6).toString());
}
//tested

void Db_handler::get_self_slot(const QString &id, const QString &type, QVariantMap *selfData, bool *hasError) {
    *hasError = false;

    if (id.isEmpty())
    {
        std::cout << "[Database::get_self]  Error: Empty parameter: id" << std::endl;
        selfData->clear();
        return;
    }

    if (type.isEmpty())
    {
        std::cout << "[Database::get_self]  Error: Empty parameter: type" << std::endl;
        selfData->clear();
        return;
    }

    QString query;
    QRegExp numId;
    numId.setPattern(R"(^\d+$)");
    QRegExp email;
    email.setPattern(R"(^\w+@\w+.\w+$)");
    QRegExp neptun;
    neptun.setPattern(R"(^\w\w\w\w\w\w$)");

    if (type == "user_name" || (type == "email" && id.contains(email)) || (type == "neptun" && id.contains(neptun)))
    {
        query = type + " = '" + id +"'";
    }
    else if (type == "id" && id.contains(numId))
    {
        query = type + " = " + id;
    }
    else
    {
        std::cout << "[Database::get_self]  Error: Bad type parameter" << std::endl;
        selfData->clear();
        return;
    }

    bool ok, localError;

    check_user_slot(query, &ok, &localError);

    if (localError)
    {
        std::cout << "[Db_handler] Error: From Db_server - check_user" << std::endl;
        *hasError = true;
        return;
    }

    if (!ok)
    {
        std::cout << "[Db_handler] Error: From Db_server - No matching user" << std::endl;
        selfData->clear();
        return;
    }

    getSelfQuery.clear();

    if(!getSelfQuery.exec("SELECT name, user_name, email, neptun, department_id FROM user WHERE " + query))
    {
        std::cout << "[Database::get_self]  Error: " << getSelfQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    getSelfQuery.next();

    selfData->insert("name", getSelfQuery.value(0).toString());
    selfData->insert("user_name", getSelfQuery.value(1).toString());
    selfData->insert("email", getSelfQuery.value(2).toString());
    selfData->insert("neptun", getSelfQuery.value(3).toString());
    selfData->insert("department_id", getSelfQuery.value(4).toString());
}
//tested

void Db_handler::check_admin_slot(const QString &user_name, const QString &password, bool *ok, bool *hasError) {
    *ok = true;
    *hasError = false;

    checkAdminQuery.clear();

    if(!checkAdminQuery.exec("SELECT COUNT(*) FROM  user JOIN department ON user.department_id=department.id"
                             " WHERE user_name = '" + user_name + "' AND  password = '" + password + "' AND department.id = 1"))
    {
        std::cout << "[Database::check_admin]  Error: " << checkAdminQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    checkAdminQuery.next();

    if (checkAdminQuery.value(0).toInt() == 0)
    {
        *ok = false;
        return;
    }
}
//tested

void Db_handler::get_other_slot(const QString &id, const QString &type, QVariantMap *selfData, bool *hasError) {
    *hasError = false;

    if (id.isEmpty())
    {
        std::cout << "[Database::get_self]  Error: Empty parameter: id" << std::endl;
        selfData->clear();
        return;
    }

    if (type.isEmpty())
    {
        std::cout << "[Database::get_self]  Error: Empty parameter: type" << std::endl;
        selfData->clear();
        return;
    }

    QString query;
    QRegExp numId;
    numId.setPattern(R"(^\d+$)");
    QRegExp neptun;
    neptun.setPattern(R"(^\w\w\w\w\w\w$)");

    if (type == "user_name" || (type == "neptun" && id.contains(neptun)))
    {
        query = type + " = '" + id +"'";
    }
    else if (type == "id" && id.contains(numId))
    {
        query = type + " = " + id;
    }
    else
    {
        std::cout << "[Database::get_self]  Error: Bad type parameter" << std::endl;
        selfData->clear();
        return;
    }

    bool ok, localError;

    check_user_slot(query, &ok, &localError);

    if (localError)
    {
        std::cout << "[Db_handler] Error: From Db_server - check_user" << std::endl;
        *hasError = true;
        return;
    }

    if (!ok)
    {
        std::cout << "[Db_handler] Error: From Db_server - No matching user" << std::endl;
        selfData->clear();
        return;
    }

    getOtherQuery.clear();

    if(!getOtherQuery.exec("SELECT user_name, neptun, department_id FROM user WHERE id = " + id))
    {
        std::cout << "[Database::get_other]  Error: " << getOtherQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    getOtherQuery.next();

    selfData->insert("user_name", getOtherQuery.value(0).toString());
    selfData->insert("neptun", getSelfQuery.value(1).toString());
    selfData->insert("department_id", getSelfQuery.value(2).toString());
}
//tested

void Db_handler::get_login_slot(const QString &id, const QString &passw, const QString &type, bool *ok, bool *hasError) {
    *ok = true;
    *hasError = false;

    if (id.isEmpty())
    {
        std::cout << "[Database::get_login]  Error: Empty parameter: id" << std::endl;
        *ok = false;
        return;
    }

    if (passw.isEmpty())
    {
        std::cout << "[Database::get_login]  Error: Empty parameter: password" << std::endl;
        *ok = false;
        return;
    }

    if (type.isEmpty())
    {
        std::cout << "[Database::get_login]  Error: Empty parameter: type" << std::endl;
        *ok = false;
        return;
    }

    if (type != "user_name" && type != "email")
    {
        std::cout << "[Database::get_login]  Error: Invalid type" << std::endl;
        *hasError = true;
        return;
    }

    QRegExp email;
    email.setPattern(R"(^\S+@\S+.\S+$)");

    if (type == "user_name" && id.contains(email))
    {
        std::cout << "[Database::get_login]  Error: Invalid type for id (user_name)" << std::endl;
        *hasError = true;
        return;
    }

    if (type == "email" && !id.contains(email))
    {
        std::cout << "[Database::get_login]  Error: Invalid type for id (email)" << std::endl;
        *hasError = true;
        return;
    }

    QString query = type + " = '" + id + "' AND password = '" + passw + "'";

    getLoginQuery.clear();

    if(!getLoginQuery.exec("SELECT COUNT(*) FROM user WHERE " + query))
    {
        std::cout << "[Database::get_login]  Error: " << getLoginQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    getLoginQuery.next();

    if (getLoginQuery.value(0).toInt() == 0)
    {
        *ok = false;
        return;
    }
}
//tested

void Db_handler::get_id_slot(const QString &table, const QString &parameter, const QString &type, QString *id, bool *hasError) {
    *id = "";
    *hasError = false;

    if (parameter.isEmpty())
    {
        return;
    }

    getUserByNameQuery.clear();

    if(!getUserByNameQuery.exec("SELECT id FROM " + table + " WHERE " + type + " = '" + parameter + "'"))
    {
        std::cout << "[Database::get_user_id_by_" + type.toStdString() + "]  Error: " << getUserByNameQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    getUserByNameQuery.next();

    if (getUserByNameQuery.value(0).toString().isEmpty())
    {
        std::cout << "[Database::get_user_id_by_" + type.toStdString() + "]  Error: Empty id for user: " + parameter.toStdString() << std::endl;
        *id = "-1";
        return;
    }

    *id = getUserByNameQuery.value(0).toString();
}
//tested

void Db_handler::check_reg_slot(const QString &user_name, const QString &email, const QString &neptun, QString *error, bool *hasError) {
    *error = "";
    *hasError = false;

    if (user_name.isEmpty())
    {
        *error = "empty_user_name";
        return;
    }
    if (email.isEmpty())
    {
        *error = "empty_email";
        return;
    }
    if (neptun.isEmpty())
    {
        *error = "empty_neptun";
        return;
    }

    checkRegQuery.clear();

    if(!checkRegQuery.exec("SELECT COUNT(*) FROM user WHERE user_name = '" + user_name + "'"))
    {
        std::cout << "[Database::check_reg - user_name]  Error: " << checkRegQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    checkRegQuery.next();

    if (checkRegQuery.value(0).toInt() > 0)
    {
        *error = "user_name";
        return;
    }

    checkRegQuery.clear();

    if(!checkRegQuery.exec("SELECT COUNT(*) FROM user WHERE email = '" + email + "'"))
    {
        std::cout << "[Database::check_reg - email]  Error: " << checkRegQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    checkRegQuery.next();

    if (checkRegQuery.value(0).toInt() > 0)
    {
        *error = "email";
        return;
    }

    checkRegQuery.clear();

    if(!checkRegQuery.exec("SELECT COUNT(*) FROM user WHERE neptun = '" + neptun + "'"))
    {
        std::cout << "[Database::check_reg - neptun]  Error: " << checkRegQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    checkRegQuery.next();

    if (checkRegQuery.value(0).toInt() > 0)
    {
        *error = "neptun";
        return;
    }
}
//tested

void Db_handler::add_department_slot(const QString &dep_name, bool *hasError) {
    if (dep_name.isEmpty())
    {
        *hasError = true;
        return;
    }

    std::lock_guard<std::mutex> m(db_m);

    *hasError = false;

    dbWriteQuery.clear();

    if(!dbWriteQuery.exec("INSERT INTO department (name) VALUES ('" + dep_name + "')"))
    {
        std::cout << "[Database::add_department]  Error: " << dbWriteQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }
}
//tested

void Db_handler::set_dep_id_slot(const QString &user_id, const QString &dep_id, bool *hasError) {
    if (user_id.isEmpty() || dep_id.isEmpty())
    {
        *hasError = true;
        return;
    }

    bool localOk, localError;

    validate_dep_id_slot(dep_id, &localOk, &localError);

    if (!localOk || localError)
    {
        *hasError = true;
        return;
    }

    *hasError = false;

    dbWriteQuery.clear();

    if(!dbWriteQuery.exec("UPDATE user SET department_id = " + dep_id + " WHERE id = " + user_id))
    {
        std::cout << "[Database::set_dep_id]  Error: " << dbWriteQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }
}
//tested

void Db_handler::set_password_slot(const QString &user_id, const QString &user_name, const QString &password, bool *hasError) {
    if (user_id.isEmpty() || user_name.isEmpty() || password.isEmpty())
    {
        *hasError = true;
        return;
    }

    *hasError = false;

    QString localId;
    bool localError;

    get_id_slot("user", user_name, "user_name", &localId, &localError);

    if (localError || localId != user_id)
    {
        *hasError = true;
        return;
    }

    dbWriteQuery.clear();

    if(!dbWriteQuery.exec("UPDATE user SET password = '" + password + "' WHERE id = " + user_id + " AND user_name = '" + user_name + "'"))
    {
        std::cout << "[Database::set_password]  Error: " << dbWriteQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }
}

void Db_handler::set_reg_slot(const QString &name, const QString &user_name, const QString &password, const QString &email, const QString &neptun, const QString &dep_id, bool *hasError) {
    std::lock_guard<std::mutex> m(db_m);

    if (name.isEmpty() || user_name.isEmpty() || password.isEmpty() ||
        email.isEmpty() || neptun.isEmpty() || dep_id.isEmpty())
    {
        *hasError = true;
        return;
    }

    *hasError = false;

    dbWriteQuery.clear();

    if(!dbWriteQuery.exec("INSERT INTO user (name, user_name, password, email, neptun, department_id)"
                          "VALUES ('" + name + "', '" + user_name + "', '" + password + "', '" + email + "', '" + neptun + "', " + dep_id + ")"))
    {
        std::cout << "[Database::set_reg]  Error: " << dbWriteQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    *hasError = false;
}


void Db_handler::clear_db_slot(QVariantList *users, QVariantList *deps, bool *hasError) {
    *hasError = false;

    dbWriteQuery.clear();

    if(!dbWriteQuery.exec("SELECT id FROM user WHERE (id not like 1) and (not id = 2)"))
    {
        std::cout << "[Database::clear_db_1]  Error: " << dbWriteQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    while (dbWriteQuery.next()) {
        users->push_back(dbWriteQuery.value(0));
    }

    dbWriteQuery.clear();

    if(!dbWriteQuery.exec("SELECT id FROM department WHERE (id not like 1) and (not id = 2)"))
    {
        std::cout << "[Database::clear_db_2]  Error: " << dbWriteQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    while (dbWriteQuery.next()) {
        deps->push_back(dbWriteQuery.value(0));
    }

    dbWriteQuery.clear();

    if(!dbWriteQuery.exec("DELETE FROM user WHERE (id not like 1) and (not id = 2)"))
    {
        std::cout << "[Database::clear_db_3]  Error: " << dbWriteQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }

    dbWriteQuery.clear();

    if(!dbWriteQuery.exec("DELETE FROM department WHERE (id not like 1)"))
    {
        std::cout << "[Database::clear_db_4]  Error: " << dbWriteQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }
}

void Db_handler::delete_from_db_slot(const QString &table, const QString &query, bool *hasError) {
    *hasError = false;

    dbWriteQuery.clear();

    if(!dbWriteQuery.exec("DELETE From " + table + " WHERE " + query))
    {
        std::cout << "[Database::delete_from_db]  Error: " << dbWriteQuery.lastError().text().toStdString() << std::endl;
        *hasError = true;
        return;
    }
}
