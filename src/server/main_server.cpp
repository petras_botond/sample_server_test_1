//
// Created by petras on 2020. 05. 27..
//

#include "main_server.h"

#include <utility>

Main_server::Main_server(const QString& route):
managerServer(route)
{
    Get(R"(/)",[&](const Request& req, Response& res) {managerServer.temp(req,res);});
    Get(R"(/user/(\d+))",[&](const Request& req, Response& res) {managerServer.getData(req,res);});

    Post(R"(/dp)",[&](const Request& req, Response& res) {managerServer.addDepartment(req,res);});

    Post(R"(/user/passw)",[&](const Request& req, Response& res) {managerServer.setPw(req,res);});
    Post(R"(/user/dp/(\d+))",[&](const Request& req, Response& res) {managerServer.setDepartment(req,res);});
    Post(R"(/user/registration)",[&](const Request& req, Response& res) {managerServer.registration(req,res);});

    Post(R"(/dp/all)",[&](const Request& req, Response& res) {managerServer.all(req,res,"department");});

    Post(R"(/user/all)",[&](const Request& req, Response& res) {managerServer.all(req,res,"user");});
    Post(R"(/user/all/(\d+))",[&](const Request& req, Response& res) {managerServer.getUserAll(req,res);});
    Post(R"(/user/self/(\d+))",[&](const Request& req, Response& res) {managerServer.getSelfData(req,res);});
    Post(R"(/user/login)",[&](const Request& req, Response& res) {managerServer.login(req,res, "user_name");});
    Post(R"(/user/login/email)",[&](const Request& req, Response& res) {managerServer.login(req,res, "email");});

    Post(R"(/dp/delete)",[&](const Request& req, Response& res) {managerServer.deleteFromDb(req,res,"department");});

    Post(R"(/clear)",[&](const Request& req, Response& res) {managerServer.clearDb(req,res);});
    Post(R"(/user/delete/(\d+))",[&](const Request& req, Response& res) {managerServer.delSelfAcc(req,res);});
    Post(R"(/user/delete)",[&](const Request& req, Response& res) {managerServer.deleteFromDb(req,res,"user");});

//    Put(R"()",[&](const Request& req, Response& res) {});
//    Delete(R"()",[&](const Request& req, Response& res) {});

    std::cout << "[Main_server] Log: Start server" << std::endl;

}
