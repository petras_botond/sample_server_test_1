//
// Created by petras on 2020. 05. 27..
//

#pragma once

#include <iostream>

#include <QString>

#include <httplib.h>

#include "manager_server/manager_server.h"

class Main_server : public httplib::Server{
public:
    Main_server(const QString& route);

protected:
    Manager_server managerServer;
};
