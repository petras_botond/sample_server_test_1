//
// Created by petras on 2020. 03. 13..
//

#pragma once

#include <QObject>

#include <httplib.h>

#include <database/db_handler.h>

using namespace httplib;

class Manager_server :public QObject{
Q_OBJECT
public:
    explicit Manager_server(const QString& dbName);

////////////////////////////////////////////////////////////
//
//  Parameter prepare functions exec instances
//
////////////////////////////////////////////////////////////

    void temp(const Request &request, Response &response);

    void setPw(const Request &request, Response &response);
    void getData(const Request &request, Response &response);
    void clearDb(const Request &request, Response &response);
    void getUserAll(const Request &request, Response &response);
    void delSelfAcc(const Request &request, Response &response);
    void getSelfData(const Request &request, Response &response);
    void registration(const Request &request, Response &response);
    void addDepartment(const Request &request, Response &response);
    void setDepartment(const Request &request, Response &response);

    void all(const Request &request, Response &response, const QString &type);
    void login(const Request &request, Response &response, const QString &type);
    void deleteFromDb(const Request &request, Response &response, const QString &table);

protected:

    static bool contentChecker(const Request &request, Response &response);

signals:

////////////////////////////////////////////////////////////
//
//  Signal calls for execute
//
////////////////////////////////////////////////////////////

    void get_all_user(QVariantList *data, bool *hasError);
    void get_all_department(QVariantList *data, bool *hasError);
    void check_user(const QString &query, bool *ok, bool *hasError);
    void validate_dep_id(const QString &id, bool *ok, bool *hasError);
    void get_user_all(const QString &id, QVariantMap *data, bool *hasError);
    void get_self(const QString &id, const QString &type, QVariantMap *selfData, bool *hasError);
    void check_admin(const QString &user_name, const QString &password, bool *ok, bool *hasError);
    void get_other(const QString &id, const QString &type, QVariantMap *selfData, bool *hasError);
    void get_login(const QString &id, const QString &passw, const QString &type, bool *ok, bool *hasError);
    void get_id(const QString &table,const QString &parameter, const QString &type, QString *id, bool *hasError);
    void check_reg(const QString &user_name, const QString &email, const QString &neptun, QString *error, bool *hasError);

    void add_department(const QString &dep_name, bool *hasError);
    void set_dep_id(const QString &user_id, const QString &dep_id, bool *hasError);
    void set_password(const QString &user_id, const QString &user_name, const QString &password, bool *hasError);
    void set_reg(const QString &name, const QString &user_name, const QString &password, const QString &email, const QString &neptun, const QString &dep_id, bool *hasError);

    void clear_db(QVariantList *ids, QVariantList *deps, bool *hasError);
    void delete_from_db(const QString &table, const QString &query, bool *hasError);

protected:

////////////////////////////////////////////////////////////
//
//  Local class variables
//
////////////////////////////////////////////////////////////

    Db_handler *db;

};
