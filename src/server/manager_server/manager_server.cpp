//
// Created by petras on 2020. 03. 13..
//

#include <iostream>

#include <QJsonDocument>

#include "manager_server.h"

Manager_server::Manager_server(const QString& dbName) {

////////////////////////////////////////////////////////////
//
//  Create a local exec instances
//  to call execution functions
//
////////////////////////////////////////////////////////////

    db = new Db_handler("QSQLITE", "Sample", dbName);
    db->init();

////////////////////////////////////////////////////////////
//
//  Connect call signals to exec instances
//
////////////////////////////////////////////////////////////

    QObject::connect(this, &Manager_server::check_reg, db, &Db_handler::check_reg_slot, Qt::ConnectionType::BlockingQueuedConnection);
    QObject::connect(this, &Manager_server::check_admin, db, &Db_handler::check_admin_slot, Qt::ConnectionType::BlockingQueuedConnection);
    QObject::connect(this, &Manager_server::get_id, db, &Db_handler::get_id_slot, Qt::ConnectionType::BlockingQueuedConnection);
    QObject::connect(this, &Manager_server::validate_dep_id, db, &Db_handler::validate_dep_id_slot, Qt::ConnectionType::BlockingQueuedConnection);
    QObject::connect(this, &Manager_server::check_user, db, &Db_handler::check_user_slot, Qt::ConnectionType::BlockingQueuedConnection);
    QObject::connect(this, &Manager_server::get_login, db, &Db_handler::get_login_slot, Qt::ConnectionType::BlockingQueuedConnection);
    QObject::connect(this, &Manager_server::get_self, db, &Db_handler::get_self_slot, Qt::ConnectionType::BlockingQueuedConnection);
    QObject::connect(this, &Manager_server::get_other, db, &Db_handler::get_other_slot, Qt::ConnectionType::BlockingQueuedConnection);
    QObject::connect(this, &Manager_server::get_all_user, db, &Db_handler::get_all_user_slot, Qt::ConnectionType::BlockingQueuedConnection);
    QObject::connect(this, &Manager_server::get_user_all, db, &Db_handler::get_user_all_slot, Qt::ConnectionType::BlockingQueuedConnection);
    QObject::connect(this, &Manager_server::get_all_department, db, &Db_handler::get_all_department_slot, Qt::ConnectionType::BlockingQueuedConnection);

    QObject::connect(this, &Manager_server::set_reg, db, &Db_handler::set_reg_slot, Qt::ConnectionType::BlockingQueuedConnection);
    QObject::connect(this, &Manager_server::add_department, db, &Db_handler::add_department_slot, Qt::ConnectionType::BlockingQueuedConnection);
    QObject::connect(this, &Manager_server::set_dep_id, db, &Db_handler::set_dep_id_slot, Qt::ConnectionType::BlockingQueuedConnection);
    QObject::connect(this, &Manager_server::set_password, db, &Db_handler::set_password_slot, Qt::ConnectionType::BlockingQueuedConnection);

    QObject::connect(this, &Manager_server::delete_from_db, db, &Db_handler::delete_from_db_slot, Qt::ConnectionType::BlockingQueuedConnection);
    QObject::connect(this, &Manager_server::clear_db, db, &Db_handler::clear_db_slot, Qt::ConnectionType::BlockingQueuedConnection);

    //QObject::connect(this, &Manager_server::check_reg, db, &Db_server::check_reg_slot, Qt::ConnectionType::QueuedConnection);

    std::cout << "[Auction_db_server] Log: Start server & Start Db_server & Start Auction_closer" << std::endl;
}

////////////////////////////////////////////////////////////
//
//  Local functions for prepare signal calls
//
////////////////////////////////////////////////////////////

void Manager_server::temp(const Request &request, Response &response) {

}


void Manager_server::setPw(const Request &request, Response &response) {
    if (!contentChecker(request,response))
    {
        return;
    }
    std::cout << "[Manager_server] Log: Request body Content-Type - OK" << std::endl;

    QVariantMap body = QJsonDocument::fromJson(QByteArray::fromStdString(request.body)).toVariant().toMap();

    if (!body.contains("user_name") || !body.contains("old_passw") || !body.contains("new_passw"))
    {
        std::cout << "[Manager_server] Error: Missing request body components" << std::endl;
        response.status = 400;
        return;
    }

    bool ok, hasError;

    emit get_login(body["user_name"].toString(), body["old_passw"].toString(), "user_name", &ok, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - get_login" << std::endl;
        response.status = 500;
        return;
    }

    if (!ok)
    {
        std::cout << "[Manager_server] Log: Invalid login" << std::endl;
        response.status = 404;
        return;
    }

    QString user_id;

    emit get_id("user", body["user_name"].toString(), "user_name", &user_id, &hasError);

    if (hasError || user_id == "-1")
    {
        std::cout << "[Manager_server] Error: From Db_server - get_user_id" << std::endl;
        response.status = 500;
        return;
    }

    emit set_password(user_id, body["user_name"].toString(), body["new_passw"].toString(), &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - set_passwprd" << std::endl;
        response.status = 500;
        return;
    }

    response.status = 200;
}

void Manager_server::getData(const Request &request, Response &response) {
    QString userId = QString::fromStdString(request.matches[1].str());

    bool hasError;
    QVariantMap selfData;

    emit get_other(userId, "id", &selfData, &hasError);

    if (!selfData.contains("user_name") || !selfData.contains("neptun") || selfData.contains("department_id"))
    {
        std::cout << "[Manager_server] Error: Missing response body components" << std::endl;
        response.status = 500;
        return;
    }

    response.set_content(QJsonDocument::fromVariant(selfData).toJson().toStdString(),"application/json");
    response.status = 200;
}

void Manager_server::clearDb(const Request &request, Response &response) {
    if (!contentChecker(request,response))
    {
        return;
    }
    std::cout << "[Manager_server] Log: Request body Content-Type - OK" << std::endl;

    QVariantMap body = QJsonDocument::fromJson(QByteArray::fromStdString(request.body)).toVariant().toMap();

    if (!body.contains("user_name") || !body.contains("password"))
    {
        std::cout << "[Manager_server] Error: Missing request body components" << std::endl;
        response.status = 400;
        return;
    }

    bool ok, hasError;

    emit check_admin(body["user_name"].toString(), body["password"].toString(), &ok, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - check_admin" << std::endl;
        response.status = 500;
        return;
    }

    if (!ok)
    {
        std::cout << "[Manager_server] Error: Not admin user" << std::endl;
        response.status = 403;
        return;
    }

    QVariantList users, deps;

    emit clear_db(&users, &deps, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - clear_db" << std::endl;
        response.status = 500;
        return;
    }

    QVariantMap resBody;

    resBody.insert("user", users);
    resBody.insert("department", deps);
    resBody.insert("delete","success");

    response.set_content(QJsonDocument::fromVariant(resBody).toJson().toStdString(),"application/json");
    response.status = 200;
}

void Manager_server::getUserAll(const Request &request, Response &response) {
    QString userId = QString::fromStdString(request.matches[1].str());

    if (!contentChecker(request,response))
    {
        return;
    }
    std::cout << "[Manager_server] Log: Request body Content-Type - OK" << std::endl;

    QVariantMap body = QJsonDocument::fromJson(QByteArray::fromStdString(request.body)).toVariant().toMap();

    if (!body.contains("user_name") || !body.contains("password"))
    {
        std::cout << "[Manager_server] Error: Missing request body components" << std::endl;
        response.status = 400;
        return;
    }

    bool ok, hasError;

    emit check_admin(body["user_name"].toString(), body["password"].toString(), &ok, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - check_admin" << std::endl;
        response.status = 500;
        return;
    }

    if (!ok)
    {
        std::cout << "[Manager_server] Error: Not admin user" << std::endl;
        response.status = 403;
        return;
    }

    QVariantMap data;

    emit get_user_all(userId, &data, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - check_admin" << std::endl;
        response.status = 500;
        return;
    }

    response.set_content(QJsonDocument::fromVariant(data).toJson().toStdString(),"application/json");
    response.status = 200;
}

void Manager_server::delSelfAcc(const Request &request, Response &response) {
    QString userId = QString::fromStdString(request.matches[1].str());

    if (!contentChecker(request,response))
    {
        return;
    }
    std::cout << "[Manager_server] Log: Request body Content-Type - OK" << std::endl;

    QVariantMap body = QJsonDocument::fromJson(QByteArray::fromStdString(request.body)).toVariant().toMap();

    if (!body.contains("user_name") || !body.contains("password"))
    {
        std::cout << "[Manager_server] Error: Missing request body components" << std::endl;
        response.status = 400;
        return;
    }

    bool ok, hasError;

    emit get_login(body["user_name"].toString(), body["password"].toString(), "user_name", &ok, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - check_admin" << std::endl;
        response.status = 500;
        return;
    }

    if (!ok)
    {
        std::cout << "[Manager_server] Log: Invalid login" << std::endl;
        response.status = 403;
        return;
    }

    QString user_id;

    emit get_id("user", body["user_name"].toString(), "user_name", &user_id, &hasError);

    if (hasError || user_id == "-1")
    {
        std::cout << "[Manager_server] Error: From Db_server - get_user_id" << std::endl;
        response.status = 500;
        return;
    }

    if (userId != user_id)
    {
        std::cout << "[Manager_server] Log: User - Id mismatch" << std::endl;
        response.status = 400;
        return;
    }

    QString query = "id = " + userId;

    emit delete_from_db("user", query, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - delete_from_db" << std::endl;
        response.status = 500;
        return;
    }

    response.set_content(R"({"delete":"success"})", "application/json");
    response.status = 200;
}

void Manager_server::getSelfData(const Request &request, Response &response) {
    if (!contentChecker(request,response))
    {
        return;
    }
    std::cout << "[Manager_server] Log: Request body Content-Type - OK" << std::endl;

    QVariantMap body = QJsonDocument::fromJson(QByteArray::fromStdString(request.body)).toVariant().toMap();

    if (!body.contains("user_name") || !body.contains("password"))
    {
        std::cout << "[Manager_server] Error: Missing request body components" << std::endl;
        response.status = 400;
        return;
    }

    bool ok, hasError;

    emit get_login(body["user_name"].toString(), body["password"].toString(), "user_name", &ok, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - get_login" << std::endl;
        response.status = 500;
        return;
    }

    if (!ok)
    {
        std::cout << "[Manager_server] Log: Invalid login" << std::endl;
        response.status = 404;
        return;
    }

    QVariantMap selfData;

    emit get_self(body["user_name"].toString(), "user_name", &selfData, &hasError);

    if (selfData.isEmpty())
    {
        response.status = 400;
        return;
    }

    if (!selfData.contains("name") || !selfData.contains("user_name") || !selfData.contains("email") ||
        !selfData.contains("neptun") || selfData.contains("department_id"))
    {
        std::cout << "[Manager_server] Error: Missing response body components" << std::endl;
        response.status = 500;
        return;
    }

    response.set_content(QJsonDocument::fromVariant(selfData).toJson().toStdString(),"application/json");
    response.status = 200;
}

void Manager_server::registration(const Request &request, Response &response) {
    if (!contentChecker(request,response))
    {
        return;
    }
    std::cout << "[Manager_server] Log: Request body Content-Type - OK" << std::endl;

    QVariantMap body = QJsonDocument::fromJson(QByteArray::fromStdString(request.body)).toVariant().toMap();

    if (!body.contains("name") || !body.contains("user_name") ||
        !body.contains("password") || !body.contains("email") ||
        !body.contains("neptun") || !body.contains("department_id"))
    {
        std::cout << "[Manager_server] Error: Missing request body components" << std::endl;
        response.status = 400;
        return;
    }

    bool hasError, ok;
    QString error;

    if (body["department_id"].toString() == "1")
    {
        std::cout << "[Manager_server] Error: department_id for system" << std::endl;
        response.status = 400;
        return;
    }

    emit check_reg(body["user_name"].toString(),body["email"].toString(), body["neptun"].toString(), &error, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - check_reg" << std::endl;
        response.status = 500;
        return;
    }

    if (!error.isEmpty())
    {
        std::cout << "[Manager_server] Error: Reserved parameter: " << error.toStdString() << std::endl;
        response.status = 400;
        return;
    }

    emit validate_dep_id(body["department_id"].toString(), &ok, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - validate department id" << std::endl;
        response.status = 500;
        return;
    }

    if (!ok)
    {
        std::cout << "[Manager_server] Error: Not valid department id" << std::endl;
        response.status = 400;
        return;
    }

    emit set_reg(body["name"].toString(), body["user_name"].toString(), body["password"].toString(), body["email"].toString(), body["neptun"].toString(), body["department_id"].toString(), &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - set_reg" << std::endl;
        response.status = 500;
        return;
    }

    QString id;

    emit get_id("user", body["user_name"].toString(), "user_name", &id, &hasError);

    if (hasError || id == "-1")
    {
        std::cout << "[Manager_server] Error: From Db_server - get_user_id_by_name" << std::endl;
        response.status = 500;
        return;
    }

    response.set_content(R"({"id":")" + id.toStdString() + R"("})","application/json");
    response.status = 200;
}

void Manager_server::addDepartment(const Request &request, Response &response) {
    if (!contentChecker(request,response))
    {
        return;
    }
    std::cout << "[Manager_server] Log: Request body Content-Type - OK" << std::endl;

    QVariantMap body = QJsonDocument::fromJson(QByteArray::fromStdString(request.body)).toVariant().toMap();

    if (!body.contains("user_name") || !body.contains("password")  || !body.contains("dep_name"))
    {
        std::cout << "[Manager_server] Error: Missing request body components" << std::endl;
        response.status = 400;
        return;
    }

    bool ok, hasError;

    emit check_admin(body["user_name"].toString(), body["password"].toString(), &ok, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - check_admin" << std::endl;
        response.status = 500;
        return;
    }

    if (!ok)
    {
        std::cout << "[Manager_server] Error: Not admin user" << std::endl;
        response.status = 403;
        return;
    }

    emit add_department(body["dep_name"].toString(), &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - add_department" << std::endl;
        response.status = 500;
        return;
    }

    response.status = 200;
}

void Manager_server::setDepartment(const Request &request, Response &response) {
    QString userId = QString::fromStdString(request.matches[1].str());
    if (!contentChecker(request,response))
    {
        return;
    }
    std::cout << "[Manager_server] Log: Request body Content-Type - OK" << std::endl;

    QVariantMap body = QJsonDocument::fromJson(QByteArray::fromStdString(request.body)).toVariant().toMap();

    if (!body.contains("user_name") || !body.contains("password")  || !body.contains("dep_id"))
    {
        std::cout << "[Manager_server] Error: Missing request body components" << std::endl;
        response.status = 400;
        return;
    }

    bool ok, hasError;

    emit check_admin(body["user_name"].toString(), body["password"].toString(), &ok, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - check_admin" << std::endl;
        response.status = 500;
        return;
    }

    if (!ok)
    {
        std::cout << "[Manager_server] Error: Not admin user" << std::endl;
        response.status = 403;
        return;
    }

    emit check_user(userId, &ok, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - check_user" << std::endl;
        response.status = 500;
        return;
    }

    if (!ok)
    {
        std::cout << "[Manager_server] Error: From Db_server - No matching user" << std::endl;
        response.status = 404;
        return;
    }

    emit set_dep_id(userId, body["dep_id"].toString(), &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - set_dep_id" << std::endl;
        response.status = 500;
        return;
    }

    response.status = 200;
}


void Manager_server::all(const Request &request, Response &response, const QString &type) {
    if (!contentChecker(request,response))
    {
        return;
    }
    std::cout << "[Manager_server] Log: Request body Content-Type - OK" << std::endl;

    QVariantMap body = QJsonDocument::fromJson(QByteArray::fromStdString(request.body)).toVariant().toMap();

    if (!body.contains("user_name") || !body.contains("password"))
    {
        std::cout << "[Manager_server] Error: Missing request body components" << std::endl;
        response.status = 400;
        return;
    }

    bool ok, hasError;

    emit check_admin(body["user_name"].toString(), body["password"].toString(), &ok, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - check_admin" << std::endl;
        response.status = 500;
        return;
    }

    if (!ok)
    {
        std::cout << "[Manager_server] Error: Not admin user" << std::endl;
        response.status = 403;
        return;
    }

    QVariantList data;

    if (type == "user")
    {
        emit get_all_user(&data, &hasError);
    }
    else if (type == "department")
    {
        emit get_all_department(&data, &hasError);
    }
    else
    {
        std::cout << "[Manager_server] Error: Wrong type" << std::endl;
        response.status = 500;
        return;
    }

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - check_admin" << std::endl;
        response.status = 500;
        return;
    }

    response.set_content(QJsonDocument::fromVariant(data).toJson().toStdString(),"application/json");
    response.status = 200;
}

void Manager_server::login(const Request &request, Response &response, const QString &type) {
    if (!contentChecker(request,response))
    {
        return;
    }
    std::cout << "[Manager_server] Log: Request body Content-Type - OK" << std::endl;

    QVariantMap body = QJsonDocument::fromJson(QByteArray::fromStdString(request.body)).toVariant().toMap();

    if (type == "user_name")
    {
        if (!body.contains("user_name") || !body.contains("password"))
        {
            std::cout << "[Manager_server] Error: Missing request body components" << std::endl;
            response.status = 400;
            return;
        }
    }
    else if (type == "email")
    {
        if (!body.contains("email") || !body.contains("password"))
        {
            std::cout << "[Manager_server] Error: Missing request body components" << std::endl;
            response.status = 400;
            return;
        }
    }
    else
    {
        std::cout << "[Manager_server] Error: Invalid login type" << std::endl;
        response.status = 400;
        return;
    }

    QString id = body[type].toString(), user_id;
    bool ok, hasError;

    emit get_login(id,body["password"].toString(), type, &ok, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - get_login" << std::endl;
        response.status = 500;
        return;
    }

    if (!ok)
    {
        std::cout << "[Manager_server] Log: Invalid login" << std::endl;
        response.status = 404;
        return;
    }

    emit get_id("user", id, type, &user_id, &hasError);

    if (hasError || user_id == "-1")
    {
        std::cout << "[Manager_server] Error: From Db_server - get_user_id" << std::endl;
        response.status = 500;
        return;
    }

    response.set_content(R"({"id":")" + user_id.toStdString() + R"("})","application/json");
    response.status = 200;
}

void Manager_server::deleteFromDb(const Request &request, Response &response, const QString &table) {
    if (!contentChecker(request,response))
    {
        return;
    }
    std::cout << "[Manager_server] Log: Request body Content-Type - OK" << std::endl;

    QVariantMap body = QJsonDocument::fromJson(QByteArray::fromStdString(request.body)).toVariant().toMap();

    if (!body.contains("user_name") || !body.contains("password") ||
        !body.contains("id") || !body.contains("type"))
    {
        std::cout << "[Manager_server] Error: Missing request body components" << std::endl;
        response.status = 400;
        return;
    }

    if (table == "user") {
        if ((body["type"].toString() != "id") && (body["type"].toString() != "email") &&
            (body["type"].toString() != "user_name") && (body["type"].toString() != "neptun")) {
            std::cout << "[Manager_server] Error: Non valid id type" << std::endl;
            response.status = 400;
            return;
        }
    }
    else if (table == "department")
    {
        if ((body["type"].toString() != "id") && (body["type"].toString() != "name")) {
            std::cout << "[Manager_server] Error: Non valid id type" << std::endl;
            response.status = 400;
            return;
        }
    }
    else
    {
        std::cout << "[Manager_server] Error: Non valid database table" << std::endl;
        response.status = 500;
        return;
    }

    bool ok, hasError;

    emit check_admin(body["user_name"].toString(), body["password"].toString(), &ok, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - check_admin" << std::endl;
        response.status = 500;
        return;
    }

    if (!ok)
    {
        std::cout << "[Manager_server] Error: Not admin user" << std::endl;
        response.status = 403;
        return;
    }

    QString retId, query;

    if (body["type"].toString() == "id")
    {
        retId = body["id"].toString();

        query = "id = " + body["id"].toString();
    }
    else
    {
        emit get_id(table, body["id"].toString(), body["type"].toString(), &retId, &hasError);

        if (hasError || retId == "-1")
        {
            std::cout << "[Manager_server] Error: From Db_server - get_user_id_by_name" << std::endl;
            response.status = 500;
            return;
        }

        query = body["type"].toString() + " = '" + body["id"].toString() + "'";
    }

    emit delete_from_db(table, query, &hasError);

    if (hasError)
    {
        std::cout << "[Manager_server] Error: From Db_server - delete_from_db" << std::endl;
        response.status = 500;
        return;
    }

    QString resBody = R"({"id":")" + retId + R"(","delete":"success"})";

    response.set_content(resBody.toStdString(),"application/json");
    response.status = 200;
}


bool Manager_server::contentChecker(const Request &request, Response &response) {
    if (request.headers.find("Content-Type") == request.headers.end() || request.headers.find("Content-Type")->second != "application/json")
    {
        std::cout << "[Manager_server] Error: Missing request body Content-Type" << std::endl;
        response.status = 400;
        return false;
    }

    return true;
}
