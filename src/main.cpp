//
// Created by petras on 2020. 05. 27..
//

////////////////////////////////////////////////////////////
//
//  Main function
//
//  QCoreApplication main thread host
//
//  Basic command line interface for configure the server
//
////////////////////////////////////////////////////////////

#include <iostream>

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QString>

#include <main_server.h>

int main (int argc, char* argv[]) {

    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationVersion("0.1");

    QString ip;
    QString port;
    QString route;

////////////////////////////////////////////////////////////
//
//  Starting parameters ip & port for http server listen
//
//  Modularly expandable with other parameters
//
//  Console parameter parser
//
////////////////////////////////////////////////////////////

    QCommandLineOption portOption {
            QStringList {"p", "port"},
            "Use this port to accept incoming connections",
            "bind-port"
    };

    QCommandLineOption ipOption {
            QStringList {"i", "IP"},
            "Use this IP address to accept incoming connections",
            "bind-ip"
    };

    QCommandLineOption routeOption {
            QStringList {"r", "route"},
            "Use this route to access the database",
            "bind-route"
    };

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();

    parser.addOption(portOption);
    parser.addOption(ipOption);
    parser.addOption(routeOption);

    parser.process(app);

    ip = parser.value(ipOption);
    port = parser.value(portOption);
    route = parser.value(routeOption);

////////////////////////////////////////////////////////////
//
//  CLI for missing parameters
//
////////////////////////////////////////////////////////////

    if (ip.isEmpty() || port.isEmpty() || route.isEmpty())
    {
        std::cout << "---Welcome in Sample server CLI---" << std::endl << std::endl;

        std::string read;

        if (ip.isEmpty())
        {
            std::cout << "Please enter the listening ip for the server (to default: 0.0.0.0 type -1)" << std::endl;

            std::cin >> read;

            ip = QString::fromStdString(read);

            if (ip == "-1")
            {
                ip = "0.0.0.0";
            }
        }
        if (port.isEmpty())
        {
            std::cout << "Please enter the listening port for the server (to default: 3000 type -1)" << std::endl;

            std::cin >> read;

            port = QString::fromStdString(read);

            if (port == "-1") {
                port = "3000";
            }
        }
        if (route.isEmpty())
        {
            std::cout << "Please enter the full path to the database (not has default value)" << std::endl;

            std::cin >> read;

            route = QString::fromStdString(read);
        }
    }

////////////////////////////////////////////////////////////
//
//  Call server instance constructor with all of parameters
//
////////////////////////////////////////////////////////////

    Main_server s_server(route);

////////////////////////////////////////////////////////////
//
//  Server listening is potentially heavy
//
////////////////////////////////////////////////////////////

    std::cout << "Server listen on http://" << ip.toStdString() << ":" << port.toStdString() << std::endl;
    s_server.listen(ip.toLatin1(),port.toInt());

    return 0;
}
